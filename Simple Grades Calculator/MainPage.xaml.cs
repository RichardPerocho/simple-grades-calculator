﻿using System;
using Windows.System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;
using System.Diagnostics;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Simple_Grades_Calculator
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private double _numerator { get; set; }
        private double _denominator { get; set; }
        private double _weight { get; set; }
        Calculator studentGrade = new Calculator();
        public MainPage()
        {
            this.InitializeComponent();
        }

        private double ChangeToDouble(string input)
        {
            return Convert.ToDouble(input);
        }

        private bool IsValidInput(string text)
        {
            try
            {
                Convert.ToDouble(text);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool AreValidInputs()
        {
            var numeratorBool = IsValidInput(Numerator_TextBox.Text);
            var denominatorBool = IsValidInput(Denominator_TextBox.Text);
            var weightBool = IsValidInput(GradeWeight_TextBox.Text);
            if (numeratorBool && denominatorBool && weightBool)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void GoToDenominator_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                var input = (sender as TextBox).Text;
                if (IsValidInput(input))
                {
                    _numerator = ChangeToDouble(input);
                    Denominator_TextBox.Focus(FocusState.Programmatic);
                }
                else
                {
                    (sender as TextBox).Text = "";
                }
            }
        }

        private void GoToGradeWeight_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            var input = (sender as TextBox).Text;
            if (IsValidInput(input))
            {
                _denominator = ChangeToDouble(input);
                GradeWeight_TextBox.Focus(FocusState.Programmatic);
            }
            else
            {
                (sender as TextBox).Text = "";
            }
        }

        private void Calculate_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter && AreValidInputs())
            {
                UpdateCalculation();
            }
        }

        private void AddNewGrade_Click(object sender, RoutedEventArgs e)
        {
            if (AreValidInputs())
            {
                UpdateCalculation();
                Display_StackPanel.Children.Add(new TextBlock() { Text = "+" });
                ClearCalculationPanel();
                Calculation_StackPanel.Visibility = Visibility.Visible;

                Numerator_TextBox.Focus(FocusState.Programmatic);
            }
        }

        private void Calculate_Click(object sender, RoutedEventArgs e)
        {
            UpdateCalculation();
        }

        private void UpdateCalculation()
        {
            _numerator = ChangeToDouble(Numerator_TextBox.Text);
            _denominator = ChangeToDouble(Denominator_TextBox.Text);
            _weight = ChangeToDouble(GradeWeight_TextBox.Text);
            studentGrade.CalculateUsingFraction(_numerator, _denominator, _weight);
            TextBlock newGrade = new TextBlock() { Text = studentGrade.CurrentWeightedMark.ToString() };
            Display_StackPanel.Children.Add(newGrade);
            Overall_TextBlock.Text = studentGrade.CurrentOverall.ToString();
            Calculation_StackPanel.Visibility = Visibility.Collapsed;
        }

        private void ClearCalculationPanel()
        {
            Numerator_TextBox.ClearValue(TextBox.TextProperty);
            Denominator_TextBox.ClearValue(TextBox.TextProperty);
            GradeWeight_TextBox.ClearValue(TextBox.TextProperty);
        }

        private void ClearValues_Click(object sender, RoutedEventArgs e)
        {
            studentGrade.ClearOverall();
            ClearCalculationPanel();
            Display_StackPanel.Children.Clear();
            Calculation_StackPanel.Visibility = Visibility.Visible;
            Overall_TextBlock.ClearValue(TextBlock.TextProperty);
            Numerator_TextBox.Focus(FocusState.Programmatic);
        }
    }
}
