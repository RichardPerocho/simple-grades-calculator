﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simple_Grades_Calculator
{
    class Calculator
    {
        public double CurrentOverall { get; private set; }
        public double CurrentWeightedMark { get; private set; }

        public Calculator()
        {
            CurrentOverall = 0;
        }

        public void CalculateUsingFraction(double grade, double outOf, double weight)
        {
            if (outOf == 0) { throw new DivideByZeroException(); }
            else
            {
                var calculation = (grade / outOf) * weight;
                CurrentWeightedMark = calculation;
                CurrentOverall += calculation;
            }
        }

        public void CalculateUsingPercent(double percentGrade, double weight)
        {
            CurrentOverall += percentGrade * weight;
        }

        public void ClearOverall()
        {
            CurrentOverall = 0;
        }
    }
}
